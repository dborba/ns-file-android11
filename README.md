# Nativescirpt 8.2 file MediaStore basic project

This project contain a problem with Media store on Android 11

## My Environment

```
Angular CLI:    13.2.7
Node            14.15.0
Java JDK        11.0.14.1
Gradle          7.4.1
Windows         11
Android API 30  11
```

## Android 11+ file handling in NS 8.2

"If you had been targeting newer Android SDK's or devices you may have run into trouble with content:// uri's. 8.2 now properly handles those under the hood with the same existing file APIs."
 https://blog.nativescript.org/nativescript-8-2-announcement/index.html#android-11%2B-file-handling

## But how to handle and read content:// uri ??

Using ui-document-picker, or even imagepicker I can not get read the content of file

## Run project

`npm i`

`tns/ns run android`


## @nativescript/core: 8.2.6-alpha.0
### Downloads Folder
#### Can read all files (jpg, jpeg, png, pdf, doc, docx, xls, xlsx, csv, zip, txt, etc.), but has the wrong extension and name. eg. (msf:34)
 ```
JS: ==== CONTEXT START =========
JS: applicationContext com.tns.NativeScriptApplication@306289a
JS: selected:  content://com.android.providers.downloads.documents/document/msf:34
JS: getOrSetHelper:  org.nativescript.widgets.FileHelper@eb8652
JS: ==== CONTEXT END =========
JS:
JS: ==== READ START =========
JS: # read:  [B@a3857d9
JS: # readSync:  [B@652a04c
JS: # readText:  ����
JS: ==== READ END =========
JS:
JS: ==== SAVE START =========
JS: # saved file:  {
JS:   "_path": "/data/user/0/org.nativescript.nsfileandroid11/files/msf:34",
JS:   "_name": "msf:34",
JS:   "_extension": ".nsfileandroid11/files/msf:34"
JS: }
JS: # save size:  13120
JS: ==== SAVE END =========
```
### Documents Folder
#### Can read all document files (doc, docx, xls, xlsx, csv, etc.), but has the wrong extension and name. eg. (document:118)
 ```
JS: ==== CONTEXT START =========
JS: applicationContext com.tns.NativeScriptApplication@306289a
JS: selected:  content://com.android.providers.media.documents/document/document:118
JS: getOrSetHelper:  org.nativescript.widgets.FileHelper@6cb48bc
JS: ==== CONTEXT END =========
JS:
JS: ==== READ START =========
JS: # read:  [B@1f4a4cb
JS: # readSync:  [B@920bf66
JS: # readText:  PK♥♦
JS: # readAsynccResolve:  [B@5e4444b
JS: ==== READ END =========
JS:
JS: ==== SAVE START =========
JS: # saved file:  {
JS:   "_path": "/data/user/0/org.nativescript.nsfileandroid11/files/document:118",
JS:   "_name": "document:118",
JS:   "_extension": ".nsfileandroid11/files/document:118"
JS: }
JS: # save size:  25337
JS: ==== SAVE END =========
```
### SD-Card (External Storage) Folder
#### Can read image files (jpg, jpeg, png) correctly
 ```
JS: ==== CONTEXT START =========
JS: applicationContext com.tns.NativeScriptApplication@306289a
JS: selected:  content://com.android.externalstorage.documents/document/100D-3803:TestFolder/a75e8217491.jpg
JS: getOrSetHelper:  org.nativescript.widgets.FileHelper@482bd4c
JS: ==== CONTEXT END =========
JS:
JS: ==== READ START =========
JS: # read:  [B@893ce9b
JS: # readSync:  [B@4ee0f76
JS: # readText:  ����
JS: # readAsynccResolve:  [B@e45435f
JS: ==== READ END =========
JS:
JS: ==== SAVE START =========
JS: # saved file:  {
JS:   "_path": "/data/user/0/org.nativescript.nsfileandroid11/files/a75e8217491.jpg",
JS:   "_name": "a75e8217491.jpg",
JS:   "_extension": ".jpg"
JS: }
JS: # save size:  50826
JS: ==== SAVE END =========
```

#### Can not read other files (pdf, doc, docx, xls, xlsx, csv, zip, txt, etc.)
 ```
JS: ==== CONTEXT START =========
JS: applicationContext com.tns.NativeScriptApplication@306289a
JS: selected:  content://com.android.externalstorage.documents/document/100D-3803:TestFolder/liczniki.csv
JS: getOrSetHelper:  org.nativescript.widgets.FileHelper@dcc01cd
JS: ==== CONTEXT END =========
JS:
JS: ==== READ START =========
JS: # read:  null
JS: # readSync:  null
JS: # readText:  null
JS: # readAsyncRejected:  java.io.FileNotFoundException: /storage/100D-3803/TestFolder/liczniki.csv: open failed: EACCES (Permission denied)
JS: ==== READ END =========
JS:
JS: ==== SAVE START =========
JS: TypeError: Cannot read property 'length' of null
JS: # saved file:  {
JS:   "_path": "/data/user/0/org.nativescript.nsfileandroid11/files/liczniki.csv",
JS:   "_name": "liczniki.csv",
JS:   "_extension": ".csv"
JS: }
JS: # save size:  0
JS: ==== SAVE END =========
```

# Edit
## Problem solved on @nativescript/core: 8.2.6-alpha.2 !!!
```
Thanks for helping:
https://github.com/triniwiz && https://github.com/NathanWalker
```

## @nativescript/core: 8.2.6-alpha.2
### Downloads, Documents, External Storage Folder
#### Can read all files (jpg, jpeg, png, pdf, doc, docx, xls, xlsx, csv, zip, txt, etc.)
 ```
JS: ==== CONTEXT START =========
JS: applicationContext com.tns.NativeScriptApplication@b340d87
JS: selected:  content://com.android.externalstorage.documents/document/100D-3803:Moje Pliki/commit convention.docx
JS: getOrSetHelper:  org.nativescript.widgets.FileHelper@79023ef
JS: ==== CONTEXT END =========
JS:
JS: ==== READ START =========
JS: # read:  [B@60413da
JS: # readSync:  [B@7ec4901
JS: # readText:  PK♥♦
JS: # readAsynccResolve:  [B@2f71fc7
JS: ==== READ END =========
JS:
JS: ==== SAVE START =========
JS: # saved file:  {
JS:   "_path": "/data/user/0/org.nativescript.nsfileandroid11/files/commit convention.docx",
JS:   "_name": "commit convention.docx",
JS:   "_extension": ".docx"
JS: }
JS: # save size:  32768
JS: # orginal size:  16668
JS: ==== SAVE END =========
```





