import {ChangeDetectorRef, Component} from '@angular/core';
import {openFilePicker} from "@nativescript-community/ui-document-picker";
import {File, Folder, getFileAccess, knownFolders, path} from "@nativescript/core/file-system";
import {getNativeApplication} from "@nativescript/core/application";

@Component({
  selector: 'ns-app',
  templateUrl: './app.component.html',
})
export class AppComponent {

  constructor(private cd: ChangeDetectorRef) {

  }


  savedFile: File = null;

  chooseFile() {
    openFilePicker({
      extensions: ['jpg', "jpeg", 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'csv', 'zip', 'txt'],
      multipleSelection: true,
      permissions: {
        write: true,
        read: true,
        recursive: true,
        persistable: true,
      }
    }).then(
      data => {
        data.files.forEach(selected => {
          let applicationContext = getNativeApplication().getApplicationContext();
          const getOrSetHelper: org.nativescript.widgets.FileHelper = org.nativescript.widgets.FileHelper.fromString(applicationContext, selected);

          console.log('==== CONTEXT START =========');
          console.log('applicationContext', applicationContext);
          console.log('selected: ', decodeURIComponent(selected));
          console.log('getOrSetHelper: ', getOrSetHelper);
          console.log('==== CONTEXT END =========');

          console.log('==== READ START =========');
          const fileRead = getFileAccess().read(selected)
          console.log('# read: ', fileRead);

          const readSync = getFileAccess().readSync(selected)
          console.log('# readSync: ', readSync);

          const readText = getFileAccess().readText(selected);
          console.log('# readText: ', readText);

          getFileAccess().readAsync(selected).then(
            readAsyncResolve => {
              console.log('# readAsynccResolve: ', readAsyncResolve);
            },
            readAsyncRejected => {
              console.log('# readAsyncRejected: ', readAsyncRejected)
            }
          );
          console.log('==== READ END =========');

          this.saveFile(selected, readSync);
        });
      }
    )
  }

  saveFile(selected, readSync) {
    const folder: Folder = <Folder>knownFolders.documents();

    let newPath: string = path.join(folder.path, this.getFileNameFromContent(selected));

    File.fromPath(newPath).writeSync(readSync, (error) => {
      console.log(error)
    });

    this.savedFile = folder.getFile(this.getFileNameFromContent(selected));

    console.log('==== SAVE START =========');
    console.log('# saved file: ', this.savedFile);
    console.log('# save size: ', this.savedFile.size);
    console.log('# orginal size: ', getFileAccess().getFileSize(selected));
    console.log('==== SAVE END =========');
  }

  getFileNameFromContent(content: string) {
    const file: { path: string; name: string; extension: string } = getFileAccess().getFile(content);
    return decodeURIComponent(file.name).split('/').pop().toLowerCase()
  }
}
